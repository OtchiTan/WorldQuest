// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "World/WQWorldManager.h"
#include "WQGameState.generated.h"

/**
 * 
 */
UCLASS()
class WORLDQUEST_API AWQGameState : public AGameStateBase
{
	GENERATED_BODY()

	virtual void BeginPlay() override;
	
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
		
public:
	UFUNCTION(Server,Reliable)
	void Server_SpawnWorld();

	UPROPERTY(Replicated)
	AWQWorldManager* WorldManager;

	UPROPERTY(EditAnywhere)
	TSubclassOf<AWQWorldManager> WorldManagerClass;
};
