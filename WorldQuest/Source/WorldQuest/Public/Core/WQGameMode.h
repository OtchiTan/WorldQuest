// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "WQGameState.h"
#include "GameFramework/GameModeBase.h"
#include "Player/WQCharacter.h"
#include "Player/WQPlayerController.h"
#include "WQGameMode.generated.h"

UCLASS(minimalapi)
class AWQGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AWQGameMode();
};



