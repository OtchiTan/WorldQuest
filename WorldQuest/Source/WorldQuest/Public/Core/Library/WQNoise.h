// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "WQNoise.generated.h"

/**
 * 
 */
UCLASS()
class WORLDQUEST_API UWQNoise : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	static void SetSeed(int Seed);

	static TArray<float> GeneratePlanetMap(int ChunkSize, FIntVector ChunkPos, float Scale, int Octaves,float Persistance, float Lacunarity, int PlanetSize, float Amplificator);
	
	static int GetIndex2D(int x, int y, int ChunkSize);

	static int GetIndex3D(int x, int y, int z, int ChunkSize);
};
