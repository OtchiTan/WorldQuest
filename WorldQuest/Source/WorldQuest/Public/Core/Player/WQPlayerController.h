// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "WQPlayerController.generated.h"

class UWQChunkManager;

/**
 * 
 */
UCLASS()
class WORLDQUEST_API AWQPlayerController : public APlayerController
{
	GENERATED_BODY()

	AWQPlayerController();

	virtual void BeginPlay() override;
	
public:
	UPROPERTY(BlueprintReadWrite,EditAnywhere)
	UWQChunkManager* ChunkManager;
	
};
