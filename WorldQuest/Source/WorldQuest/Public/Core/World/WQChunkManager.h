#pragma once

#include "CoreMinimal.h"
#include "WQChunk.h"
#include "Components/ActorComponent.h"
#include "WQChunkManager.generated.h"

class AWQPlayerController;

UCLASS(Blueprintable, BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class WORLDQUEST_API UWQChunkManager : public UActorComponent
{
	GENERATED_BODY()
	
protected:
	virtual void BeginPlay() override;
	
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
public:	
	UWQChunkManager();

	UPROPERTY()
	AWQPlayerController* PlayerController;

	UPROPERTY(Replicated)
	TArray<AWQChunk*> Chunks;

	UPROPERTY(EditAnywhere)
	TSubclassOf<AWQChunk> ChunkClass;

	UFUNCTION(Client,Reliable)
	void Client_SpawnChunk();

	UFUNCTION(Server,reliable)
	void Server_SpawnChunk();

	UPROPERTY(EditAnywhere)
	int RenderDistance = 2;
	
	UPROPERTY(EditAnywhere)
	int ChunkSize = 10;
	
	UPROPERTY(EditAnywhere)
	int Seed = 5;
};
