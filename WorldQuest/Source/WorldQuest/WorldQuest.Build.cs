// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class WorldQuest : ModuleRules
{
	public WorldQuest(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[]
		{
			"Core", 
			"CoreUObject", 
			"Engine", 
			"InputCore", 
			"HeadMountedDisplay", 
			"NetCore", 
		});
		
		PublicDependencyModuleNames.Add("RuntimeMeshComponent");
	}
}
