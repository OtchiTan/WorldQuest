// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/Library/WQNoise.h"

void UWQNoise::SetSeed(int Seed)
{
	srand(Seed);
}

TArray<float> UWQNoise::GeneratePlanetMap(int ChunkSize, FIntVector ChunkPos, float Scale, int Octaves,float Persistance, float Lacunarity,int PlanetSize, float Amplificator)
{
	TArray<float> PlanetMap;
	PlanetMap.SetNum((ChunkSize + 1) * (ChunkSize + 1) * (ChunkSize + 1));

	float MaxPossibleHeight = 0;
	float Amplitude = 1;
	for (int i = 0; i < Octaves; ++i)
	{
		MaxPossibleHeight += Amplitude;
		Amplitude *= Persistance;
	}

	if (Scale <= 0)
		Scale = 0.0001f;

	const FVector PlanetOrigin = FVector(0);

	int ValueIndex = 0;
	for (int x = 0; x < ChunkSize + 1; ++x)
	{
		const int GlobalX = ChunkPos.X * ChunkSize + x;
		for (int y = 0; y < ChunkSize + 1; ++y)
		{
			const int GlobalY = ChunkPos.Y * ChunkSize + y;
			
			Amplitude = 1;
			float Frequency = 1;
			float NoiseHeight = 0;

			for (int i = 0; i < Octaves; ++i)
			{
				const float SampleX = GlobalX/ Scale * Frequency;
				const float SampleY = GlobalY / Scale * Frequency;

				const float PerlinValue = FMath::PerlinNoise2D(FVector2D(SampleX, SampleY)) * 2 - 1;
				NoiseHeight += PerlinValue * Amplitude;
				Amplitude *= Persistance;
				Frequency *= Lacunarity;
			}
			
			const float NormalizedHeight = (NoiseHeight + 1) / (2.0f * MaxPossibleHeight);
			
			const int DistanceMax = PlanetSize + NormalizedHeight * Amplificator; 
			
			for (int z = 0; z < ChunkSize + 1; ++z)
			{
				const int GlobalZ = ChunkPos.Z * ChunkSize + z;
				const FVector VoxelPos = FVector(GlobalX,GlobalY,GlobalZ);

				const float Distance = FVector::Distance(PlanetOrigin,VoxelPos);
				
				PlanetMap[ValueIndex] = Distance <= DistanceMax ? 0 : 1;
				ValueIndex++;
			}
		}
	}

	return PlanetMap;
}

int UWQNoise::GetIndex2D(int x, int y, int ChunkSize)
{
	return x * ChunkSize + y;
}

int UWQNoise::GetIndex3D(int x, int y, int z, int ChunkSize)
{
	return x * ChunkSize * ChunkSize + y * ChunkSize + z;
}
