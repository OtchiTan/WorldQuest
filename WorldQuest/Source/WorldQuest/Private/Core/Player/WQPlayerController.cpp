// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/Player/WQPlayerController.h"

#include "Core/World/WQChunkManager.h"

AWQPlayerController::AWQPlayerController()
{
	ChunkManager = CreateDefaultSubobject<UWQChunkManager>(TEXT("ChunkManager"));
}

void AWQPlayerController::BeginPlay()
{
	Super::BeginPlay();

}
