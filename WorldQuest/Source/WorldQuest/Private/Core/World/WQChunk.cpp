// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/World/WQChunk.h"

#include "Core/Library/WQNoise.h"
#include "Providers/RuntimeMeshProviderStatic.h"

// Sets default values
AWQChunk::AWQChunk()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	bReplicates = true;
	bOnlyRelevantToOwner = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));

	Mesh = CreateDefaultSubobject<URuntimeMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(RootComponent);

	StaticProvider = CreateDefaultSubobject<URuntimeMeshProviderStatic>(TEXT("StaticProvider"));
}

// Called when the game starts or when spawned
void AWQChunk::BeginPlay()
{
	Super::BeginPlay();
	
	Mesh->Initialize(StaticProvider);

	GenerateMap();
	
	ComputeChunk();
}

// Called every frame
void AWQChunk::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWQChunk::GenerateMap()
{
	NoiseMap = UWQNoise::GeneratePlanetMap(
		ChunkSize,
		ChunkPos,
		Scale,
		Octaves,
		Persistance,
		Lacunarity,
		PlanetSize,
		Amplificator
		);

	// Texture = UTexture2D::CreateTransient(ChunkSize,ChunkSize);
	// Texture->AddToRoot();
	// Texture->SRGB = 0;
	// Texture->SetPlatformData(new FTexturePlatformData());
	// Texture->GetPlatformData()->SizeX = MapWidth;
	// Texture->GetPlatformData()->SizeX = MapHeight;
	// Texture->GetPlatformData()->PixelFormat = PF_B8G8R8A8;
	// Texture->Filter = TF_Nearest;
	//
	// TArray<FColor> Pixels;
	// Pixels.SetNum(MapWidth* MapHeight);
	//
	// for (int y = 0; y < MapHeight; ++y)
	// {
	// 	for (int x = 0; x < MapWidth; ++x)
	// 	{
	// 		const float CurrentHeight = NoiseMap[x][y];
	// 		for (int i = 0; i < Regions.Num(); ++i)
	// 		{
	// 			if (CurrentHeight <= Regions[i].Height)
	// 			{
	// 				const int32 CurPixelIndex = y * MapWidth + x;
	// 				Pixels[CurPixelIndex] = Regions[i].Color;
	// 				break;
	// 			}
	// 		}
	// 	}
	// }
	//
	// FTexture2DMipMap* Mip = new FTexture2DMipMap();
	// Texture->GetPlatformData()->Mips.Add(Mip);
	// Mip->SizeX = MapWidth;
	// Mip->SizeY = MapHeight;
	//
	// Mip->BulkData.Lock(LOCK_READ_WRITE);
	// uint8* TextureData = static_cast<uint8*>(Mip->BulkData.Realloc(MapWidth * MapHeight * sizeof(FColor)));
	// FMemory::Memcpy(TextureData,Pixels.GetData(),sizeof(uint8) * MapHeight * MapWidth * 4);
	// Mip->BulkData.Unlock();
	//
	// Texture->UpdateResource();
	//
	// DynamicMaterial = UMaterialInstanceDynamic::Create(Material,nullptr);
	// DynamicMaterial->SetTextureParameterValue(FName(TEXT("Texture")),Texture);

	StaticProvider->SetupMaterialSlot(0,TEXT("Material"),DynamicMaterial);
}

void AWQChunk::ComputeChunk()
{
	if (!HeightCurve)
		return;
	
	Vertices.Empty();
	Triangles.Empty();
	UVs.Empty();
	
	Colors.Empty();
	Normals.Empty();
	Tangents.Empty();

	if (SurfaceLevel > 0.0f)
	{
		TriangleOrder[0] = 0;
		TriangleOrder[1] = 1;
		TriangleOrder[2] = 2;
	}
	else
	{
		TriangleOrder[0] = 2;
		TriangleOrder[1] = 1;
		TriangleOrder[2] = 0;
	}

	float Cube[8];
	
	for (int X = 0; X < ChunkSize; ++X)
	{
		for (int Y = 0; Y < ChunkSize; ++Y)
		{
			for (int Z = 0; Z < ChunkSize; ++Z)
			{
				for (int i = 0; i < 8; ++i)
				{
					Cube[i] = NoiseMap[UWQNoise::GetIndex3D(X + VertexOffset[i][0],Y + VertexOffset[i][1],Z + VertexOffset[i][2],ChunkSize + 1)];
				}

				March(X,Y,Z, Cube);
			}
		}
	}

	RenderChunk();
}

void AWQChunk::March(const int X, const int Y, const int Z, const float Cube[8])
{
	int VertexMask = 0;
	FVector EdgeVertex[12];

	//Find which vertices are inside of the surface and which are outside
	for (int i = 0; i < 8; ++i)
	{
		if (Cube[i] <= SurfaceLevel) VertexMask |= 1 << i;
	}

	const int EdgeMask = CubeEdgeFlags[VertexMask];
	
	if (EdgeMask == 0) return;
		
	// Find intersection points
	for (int i = 0; i < 12; ++i)
	{
		if ((EdgeMask & 1 << i) != 0)
		{
			const float Offset = GetInterpolationOffset(Cube[EdgeConnection[i][0]], Cube[EdgeConnection[i][1]]);

			EdgeVertex[i].X = X + (VertexOffset[EdgeConnection[i][0]][0] + Offset * EdgeDirection[i][0]);
			EdgeVertex[i].Y = Y + (VertexOffset[EdgeConnection[i][0]][1] + Offset * EdgeDirection[i][1]);
			EdgeVertex[i].Z = Z + (VertexOffset[EdgeConnection[i][0]][2] + Offset * EdgeDirection[i][2]);
		}
	}

	// Save triangles, at most can be 5
	for (int i = 0; i < 5; ++i)
	{
		if (TriangleConnectionTable[VertexMask][3 * i] < 0) break;

		FVector V1 = EdgeVertex[TriangleConnectionTable[VertexMask][3 * i]] * 100;
		FVector V2 = EdgeVertex[TriangleConnectionTable[VertexMask][3 * i + 1]] * 100;
		FVector V3 = EdgeVertex[TriangleConnectionTable[VertexMask][3 * i + 2]] * 100;

		FVector Normal = FVector::CrossProduct(V3 - V1,V2 - V1);
		FColor Color = FColor::MakeRandomColor();
		
		Normal.Normalize();

		Vertices.Append({V1, V2, V3});
		
		Triangles.Append({
			VertexCount + TriangleOrder[0],
			VertexCount + TriangleOrder[1],
			VertexCount + TriangleOrder[2]
		});

		Normals.Append({
			Normal,
			Normal,
			Normal
		});

		Colors.Append({
			Color,
			Color,
			Color
		});

		VertexCount += 3;
	}
}

float AWQChunk::GetInterpolationOffset(const float V1, const float V2) const
{
	const float Delta = V2 - V1;
	return Delta == 0.0f ? SurfaceLevel : (SurfaceLevel - V1) / Delta;
}

void AWQChunk::RenderChunk()
{
	if (Vertices.Num() > 0 && Triangles.Num() > 0)
		StaticProvider->CreateSectionFromComponents(0,0,0,Vertices,Triangles,Normals,UVs,Colors,Tangents,ERuntimeMeshUpdateFrequency::Infrequent,true);
}

