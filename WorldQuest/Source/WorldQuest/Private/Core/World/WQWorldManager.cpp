// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/World/WQWorldManager.h"

// Sets default values
AWQWorldManager::AWQWorldManager()
{
	PrimaryActorTick.bCanEverTick = true;

	bReplicates = true;
	bAlwaysRelevant = true;

}

// Called when the game starts or when spawned
void AWQWorldManager::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWQWorldManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

