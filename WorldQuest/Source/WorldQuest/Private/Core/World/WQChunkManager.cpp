// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/World/WQChunkManager.h"

#include "Core/Library/WQNoise.h"
#include "Core/Player/WQCharacter.h"
#include "Core/Player/WQPlayerController.h"
#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
UWQChunkManager::UWQChunkManager()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UWQChunkManager::BeginPlay()
{
	Super::BeginPlay();

	Client_SpawnChunk();
}

void UWQChunkManager::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	FDoRepLifetimeParams Params;
	Params.bIsPushBased = true;
	
	DOREPLIFETIME_WITH_PARAMS(UWQChunkManager,Chunks,Params);
}


// Called every frame
void UWQChunkManager::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UWQChunkManager::Client_SpawnChunk_Implementation()
{
	PlayerController = Cast<AWQPlayerController>(GetOwner());
	Server_SpawnChunk();
}

void UWQChunkManager::Server_SpawnChunk_Implementation()
{
	MARK_PROPERTY_DIRTY_FROM_NAME(UWQChunkManager,Chunks,this);
	PlayerController = Cast<AWQPlayerController>(GetOwner());

	UWQNoise::SetSeed(Seed);

	for (int x = -RenderDistance; x < RenderDistance; ++x)
	{
		for (int y = -RenderDistance; y < RenderDistance; ++y)
		{
			for (int z = -RenderDistance; z < RenderDistance; ++z)
			{
				FTransform ChunkSpawn;
				ChunkSpawn.SetLocation(FVector(x * ChunkSize * 100, y * ChunkSize * 100, z * ChunkSize * 100));
				AWQChunk* Chunk = GetWorld()->SpawnActorDeferred<AWQChunk>(ChunkClass,ChunkSpawn,PlayerController,nullptr);
				Chunk->ChunkSize = ChunkSize;
				Chunk->PlanetSize = RenderDistance * ChunkSize - ChunkSize;
				Chunk->ChunkPos = FIntVector(x,y,z);
				Chunk->FinishSpawning(ChunkSpawn);
			}
		}
	}

	AWQCharacter* Character = Cast<AWQCharacter>(PlayerController->GetPawn());
	Character->SetActorLocation(FVector(0,0,RenderDistance * ChunkSize * 100));
}

