// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/WQGameState.h"

#include "Net/UnrealNetwork.h"

void AWQGameState::BeginPlay()
{
	Super::BeginPlay();

	if (GetNetMode() == NM_ListenServer || GetNetMode() == NM_Standalone)
		Server_SpawnWorld();
}

void AWQGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	
	FDoRepLifetimeParams Params;
	Params.bIsPushBased = true;
	
	DOREPLIFETIME_WITH_PARAMS(AWQGameState,WorldManager,Params);
}

void AWQGameState::Server_SpawnWorld_Implementation()
{
	MARK_PROPERTY_DIRTY_FROM_NAME(AWQGameState,WorldManager,this);
	WorldManager = GetWorld()->SpawnActor<AWQWorldManager>(WorldManagerClass,FVector(0),FRotator(0));
}
